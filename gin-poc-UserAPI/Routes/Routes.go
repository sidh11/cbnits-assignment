package Routes

import (
	"gin-poc-UserAPI/Controllers"
	"net/http"

	"github.com/gin-gonic/gin"
)

//SetupRouter contains the group of routes
func SetupRouter() *gin.Engine {
	r := gin.Default()

	r.POST("/login", func(ctx *gin.Context) {
		token := Controllers.Login(ctx)
		if token != "" {
			ctx.JSON(http.StatusOK, gin.H{
				"token": token,
			})
		} else {
			ctx.JSON(http.StatusUnauthorized, nil)
		}
	})

	grp := r.Group("/user-api")
	{
		grp.GET("user", Controllers.GetUsers)
		grp.POST("user", Controllers.CreateUser)
		grp.GET("user/:id", Controllers.GetUserByID)
		grp.PUT("user/:id", Controllers.UpdateUser)
		grp.DELETE("user/:id", Controllers.DeleteUser)
	}
	return r
}
