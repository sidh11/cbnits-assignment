package service

type LoginService interface {
	Login(username string, password string) bool
}

type loginService struct {
	authorizedUsername string
	authorizedPassword string
}

func NewLoginService() LoginService {
	return &loginService{
		authorizedUsername: "nitin",
		authorizedPassword: "12345",
	}
}

func (service *loginService) Login(username string, password string) bool {
	// make DB call to authenticate the userName and Password
	return service.authorizedUsername == username && service.authorizedPassword == password
}
