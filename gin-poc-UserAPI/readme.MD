## POC assignment for CBNITS

Framework Used : Gin
Database Used : MYSQL

API Endpoints available:
GET    /user-api/user            --> to fetch the details of a all users available in database 
POST   /user-api/user            --> to insert a new user in the DB
GET    /user-api/user/:id        --> to fecth the details of a single user on the basis of ID
PUT    /user-api/user/:id        --> update the user details based on ID
DELETE /user-api/user/:id        --> delete the user based on the ID

Logging : Default logging used by Gin Gonic framework

STEPS to RUN :

1. Install GoLang any version
2. Download dependencies (using go get or go mod any preferred method)
    I have all the dependencies in the vendor folder
3. By default it will run on the 8080 port

Request Structure :

{
    "id" : 2,
    "name" : "nitin",
    "email" : "nitin.kumar@gmail.com",
    "phone" : "+91 7988165546",
    "address" : "Rewari",
}